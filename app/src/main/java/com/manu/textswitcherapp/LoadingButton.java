package com.manu.textswitcherapp;

/**
 * LoadingButton is for special scenario's where Progress bar need to
 * show when we doing long process
 * <p/>
 * Created by manu on 9/23/15.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class LoadingButton extends RelativeLayout {

    public static final int IN_FROM_RIGHT = 0;
    public static final int IN_FROM_LEFT = 1;
    //region Variables
    static final int DEFAULT_COLOR = android.R.color.white;
    private int mDefaultTextSize;
    private ProgressBar mProgressBar;
    private TextSwitcher mTextSwitcher;
    private String mLoadingMessage;
    private String mButtonText;
    private String mButtonVerifiedText;
    private float mTextSize;
    private int mTextColor;
    private boolean mIsLoadingShowing;
    private Typeface mTypeface;
    private Animation inRight;
    private Animation inLeft;
    private int mCurrentInDirection;
    private boolean mTextSwitcherReady;
    private boolean isBold;
    //endregion

    //region Constructors
    public LoadingButton(Context context) {
        super(context);
        init(context, null);
    }

    public LoadingButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LoadingButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }
    //endregion

    public float getTextSize() {
        return mTextSize;
    }

    private void setTextSize(float size) {
        mTextSize = size;
    }

    public Typeface getTypeface() {
        return mTypeface;
    }

    public void setTypeface(@NonNull Typeface typeface) {
        this.mTypeface = typeface;
    }

    public void setProgressColor(int colorRes) {
        mProgressBar.getIndeterminateDrawable()
                .mutate()
                .setColorFilter(colorRes, PorterDuff.Mode.SRC_ATOP);
    }

    public void setAnimationInDirection(int inDirection) {
        mCurrentInDirection = inDirection;
    }

    public void setText(String text) {
        if (text != null) {
            mButtonText = text;
            if (mTextSwitcherReady) {
                mTextSwitcher.setInAnimation(mCurrentInDirection == IN_FROM_LEFT ? inLeft : inRight);
                mTextSwitcher.setText(mButtonText);
            }
        }
    }

    public void setText(String text, boolean isBold) {
        this.isBold = isBold;
        if (text != null) {
            mButtonText = text;
            if (mTextSwitcherReady) {
                mTextSwitcher.setInAnimation(mCurrentInDirection == IN_FROM_LEFT ? inLeft : inRight);
                if (isBold)
                    mTextSwitcher.setText(convertToBold(mButtonText));
                else
                    mTextSwitcher.setText(mButtonText);
            }
        }
    }

    public void setLoadingText(String text) {
        if (text != null) {
            mLoadingMessage = text;
        }
    }

    public void setTextFactory(@NonNull ViewSwitcherFactory factory) {
        mTextSwitcher.removeAllViewsInLayout();
        mTextSwitcher.setFactory(factory);
        mTextSwitcher.setText(mButtonText);
    }

    public void showLoading() {
        Log.e("showLoading", "" + mIsLoadingShowing);
        if (!mIsLoadingShowing) {
            mProgressBar.setVisibility(View.VISIBLE);
            if (!isBold)
                mTextSwitcher.setText(mLoadingMessage);
            else
                mTextSwitcher.setText(convertToBold(mLoadingMessage));
            setEnabled(false);
            mIsLoadingShowing = true;
            Log.e("showLoading", "" + mIsLoadingShowing);
        }
    }

    public void showButtonText(String mButtonVerifiedText, boolean isBold) {
        this.isBold = isBold;
//        if (mIsLoadingShowing) {
        mProgressBar.setVisibility(View.GONE);
        if (!isBold) {
            mTextSwitcher.setCurrentText(mButtonVerifiedText);
        } else {
            mTextSwitcher.setCurrentText(convertToBold(mButtonVerifiedText));
        }
        setEnabled(true);
        mIsLoadingShowing = false;
//        Log.e("Is Loading Button", ((TextView) mTextSwitcher.getCurrentView()).getText().toString());
//        }
    }

    public boolean isLoadingShowing() {
        return mIsLoadingShowing;
    }

    private void init(Context context, AttributeSet attrs) {
        mDefaultTextSize = getResources().getDimensionPixelSize(R.dimen.text_default_size);
        mIsLoadingShowing = false;
        Log.e("init", "" + mIsLoadingShowing);
        LayoutInflater.from(getContext()).inflate(R.layout.view_loading_button, this, true);

        mProgressBar = (ProgressBar) findViewById(R.id.pb_progress);
        mTextSwitcher = (TextSwitcher) findViewById(R.id.pb_text);


        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.LoadingButton,
                    0, 0);
            try {
                float textSize = a.getDimensionPixelSize(R.styleable.LoadingButton_pbTextSize, mDefaultTextSize);
                setTextSize(textSize);

                String text = a.getString(R.styleable.LoadingButton_pbText);
                setText(text);

                mLoadingMessage = a.getString(R.styleable.LoadingButton_pbLoadingText);

                if (mLoadingMessage == null) {
                    mLoadingMessage = getContext().getString(R.string.default_loading);
                }

                int progressColor = a.getColor(R.styleable.LoadingButton_pbProgressColor,
                        getResources().getColor(DEFAULT_COLOR));
                setProgressColor(progressColor);

                int textColor = a.getColor(R.styleable.LoadingButton_pbTextColor,
                        getResources().getColor(DEFAULT_COLOR));
                setTextColor(textColor);

            } finally {
                a.recycle();
            }
        } else {
            int white = getResources().getColor(DEFAULT_COLOR);
            mLoadingMessage = getContext().getString(R.string.default_loading);
            setProgressColor(white);
            setTextColor(white);
            setTextSize(mDefaultTextSize);
        }
        setupTextSwitcher();
    }

    private void setupTextSwitcher() {
        ViewSwitcherFactory factory = new ViewSwitcherFactory(getContext(), mTextColor, mTextSize, mTypeface);
        mTextSwitcher.setFactory(factory);

        inRight = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_right);
        inLeft = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        mTextSwitcher.setOutAnimation(out);
        mTextSwitcher.setInAnimation(inRight);

        mTextSwitcher.setText(mButtonText);
        mTextSwitcherReady = true;
    }

    private void setTextColor(int textColor) {
        this.mTextColor = textColor;
    }

    private SpannableString convertToBold(String src) {
        SpannableString spanText = new SpannableString(src);
        spanText.setSpan(new StyleSpan(Typeface.BOLD), 0, src.length(), 0);
        return spanText;

    }

    public static class ViewSwitcherFactory implements ViewSwitcher.ViewFactory {

        //region Variables
        private final int textColor;
        private final float textSize;
        private final Typeface typeFace;
        private final Context context;
        //endregion

        //region Constructor
        public ViewSwitcherFactory(Context context, int textColor, float textSize, Typeface typeface) {
            this.context = context;
            this.textColor = textColor;
            this.textSize = textSize;
            this.typeFace = typeface;
        }
        //endregion

        @Override
        public View makeView() {
            TextView tv = new TextView(context);
            tv.setTextColor(textColor);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv.setGravity(Gravity.CENTER);
            tv.setTypeface(typeFace);

            return tv;
        }
    }
}